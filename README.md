<h1 align="center">Hi 👋, I'm Bešlagić Adin</h1>
<h3 align="center"> 📖 Student of informatics and computer science 🖥️⌨️🖱️</h3>

<p align="center">
  <img src="https://komarev.com/ghpvc/?username=beslagicadin&label=Profile%20views&color=0e75b6&style=flat" alt="beslagicadin" />
</p>

<h3 align="center">🔗 Connect with me:</h3>
<p align="center">
  <a href="https://www.linkedin.com/in/beslagicadin/" target="blank"><img align="center" src="https://image.flaticon.com/icons/png/512/174/174857.png" alt="Adin Bešlagić LinkedIn" height="60" width="60" /></a>
  <a href="https://www.facebook.com/beslagicadin/" target="blank"><img align="center" src="https://image.flaticon.com/icons/png/512/1384/1384053.png" alt="Adin Bešlagić Facebook" height="60" width="60" /></a>
  <a href="https://www.instagram.com/beslagicadin/" target="blank"><img align="center" src="https://user-images.githubusercontent.com/62249321/117031986-679f5580-ad01-11eb-9a62-29184c190f86.png" alt="Adin Bešlagić Instagram" height="60" width="60" /></a>
  <a href="mailto:beslagicadin@gmail.com" target="blank"><img align="center" src="https://www.google.com/gmail/about/static/images/logo-gmail.png?cache=1adba63" alt="Adin Bešlagić Instagram" height="60" width="60" /></a>
  <br><br>
</p>
<h3 align="center">🌐Languages and Tools🔧</h3>
<p align="center">
  <a href="https://www.java.com" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" height="60" width="60" /> </a>
  <a href="https://developer.android.com" target="_blank"> <img src="https://1.bp.blogspot.com/-LgTa-xDiknI/X4EflN56boI/AAAAAAAAPuk/24YyKnqiGkwRS9-_9suPKkfsAwO4wHYEgCLcBGAsYHQ/s0/image9.png" alt="androidStudio" height="65" width="65" /> </a>
  <a href="https://www.jetbrains.com/idea/" target="_blank"> <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/IntelliJ_IDEA_Icon.svg/768px-IntelliJ_IDEA_Icon.svg.png" alt="inteliJ" height="60" width="60" /> </a>
  <br>
  <a href="https://www.cprogramming.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" height="60" width="60" /> </a>
  <a href="https://www.w3schools.com/cpp/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" height="60" width="60" /> </a>
  <a href="https://www.w3schools.com/cs/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/csharp/csharp-original.svg" alt="csharp" height="60" width="60" /> </a>
  <br>
  <a href="https://www.w3.org/html/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" height="60" width="60" /> </a>
  <a href="https://www.w3schools.com/css/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" height="60" width="60" /> </a>
  <a href="https://code.visualstudio.com/" target="_blank"> <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/512px-Visual_Studio_Code_1.35_icon.svg.png" alt="qt" height="60" width="60" /> </a>
  <br> 
  <a href="https://www.qt.io/" target="_blank"> <img src="https://upload.wikimedia.org/wikipedia/commons/0/0b/Qt_logo_2016.svg" alt="qt" height="60" width="60" /> </a> 
  <a href="https://visualstudio.microsoft.com/" target="_blank"> <img src="https://image.flaticon.com/icons/png/512/906/906324.png" alt="qt" height="60" width="60" /> </a> 
  <br>
  <a href="https://www.linux.org/" target="_blank"> <img src="https://img.icons8.com/nolan/64/linux--v2.png" alt="linux" height="60" width="60" /> </a>
  <a href="https://www.microsoft.com/en-us/windows/" target="_blank"> <img src="https://image.flaticon.com/icons/png/512/732/732225.png" alt="windows10" height="60" width="60" /> </a>
  <a href="https://www.kali.org/" target="_blank"> <img src="https://img.icons8.com/color/50/000000/kali-linux.png" alt="git" height="60" width="70" /> </a>
  <br>
  <a href="https://git-scm.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" height="60" width="60" /> </a>
</p>
<br>
  <p align="center">
    <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs?username=beslagicadin&show_icons=true&locale=en&layout=compact" alt="beslagicadin"/>
</p>
  <br>
<p align="center">
  <img align="center" src="https://github-readme-stats.vercel.app/api?username=beslagicadin&show_icons=true&locale=en" alt="beslagicadin" />
</p>
  <br>  <br>  <br>
  <footer> <small>&copy; Copyright 2021, Adin Bešlagić</small> </footer> 
